package com.abscbn.myxph;

import java.lang.ref.SoftReference;
import java.util.HashMap;

import android.graphics.Bitmap;

public class MemoryCache {
    private HashMap<String, SoftReference<Bitmap>> cache=new HashMap<String, SoftReference<Bitmap>>();
    private HashMap<String, SoftReference<Charts>> dataCache=new HashMap<String, SoftReference<Charts>>();
    
    public Bitmap get(String id){
        if(!cache.containsKey(id))
            return null;
        SoftReference<Bitmap> ref=cache.get(id);
        return ref.get();
    }
    
    public Charts getData(String id){
        if(!dataCache.containsKey(id))
            return null;
        SoftReference<Charts> ref=dataCache.get(id);
        return ref.get();
    }
    
    public void put(String id, Bitmap bitmap){
        cache.put(id, new SoftReference<Bitmap>(bitmap));
    }
    
    public void putData(String id, Charts data){
        dataCache.put(id, new SoftReference<Charts>(data));
    }

    public void clear() {
        cache.clear();       
    }
    
    
    public void clearData() {
        dataCache.clear();
    }
}