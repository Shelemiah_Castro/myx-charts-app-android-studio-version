package com.abscbn.myxph;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import android.content.Context;

public class ChartLoader {
	public static enum indexType {
		max, min
	}

	private static ChartCacheIndex cci = new ChartCacheIndex();
	private static MemoryCache memoryCache = new MemoryCache();
	private static FileCache fileCache;
	private static boolean idxLoaded = false;
	public static Charts myxData;

	public static void init(Context context) {
		fileCache = new FileCache(context);
	}

	public static void SetChartMinMax(String min, String max) {
		cci.minIndex = min;
		cci.maxIndex = max;

		File f = fileCache.getFile("ChartMinMax.str");
		try {
			OutputStream fos = new FileOutputStream(f);
			ObjectOutputStream out = new ObjectOutputStream(fos);
			out.writeObject(cci);
			out.close();
			fos.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public static String GetChartMinMax(indexType it) {
		String i = "";

		try {

			if (!idxLoaded) {
				File f = fileCache.getFile("ChartMinMax.str");
				if (f.exists()) {
					ObjectInputStream in = null;
					FileInputStream fis = new FileInputStream(f);
					in = new ObjectInputStream(fis);
					cci = (ChartCacheIndex) in.readObject();
					idxLoaded = true;
					in.close();
					fis.close();
				}
			}

			if (it == indexType.max)
				i = cci.maxIndex;
			else
				i = cci.minIndex;

		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		}
		return i;
	}

	public static void SetChartData(String cd, Charts data) {
		SetChartMinMax(data.minDate, data.maxDate);
		File f = fileCache.getFile("ChartDate-" + cd + ".str");
		try {
			OutputStream fos = new FileOutputStream(f);
			ObjectOutputStream out = new ObjectOutputStream(fos);
			out.writeObject(data);
			out.close();
			fos.close();
			idxLoaded = true;
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public static Charts GetChartData(String cd,boolean setAsCurrent) {
		// chartViews.put(data, url);
		Charts cData = memoryCache.getData(cd);
		if (cData == null) {
			try {
				File f = fileCache.getFile("ChartDate-" + cd + ".str");
				if (f.exists())
				{
				   ObjectInputStream in = null;
				   FileInputStream fis = new FileInputStream(f);
			   	   in = new ObjectInputStream(fis);
				   cData = (Charts) in.readObject();
				   memoryCache.putData(cd, cData);
				   in.close();
				   fis.close();
				}
				else{
				   cData=null;
				}					

			} catch (IOException ex) {
				ex.printStackTrace();
			} catch (ClassNotFoundException ex) {
				ex.printStackTrace();
			}
		}
		
		if (setAsCurrent && cData!=null)
		   myxData = cData;
		
		return cData;
	}

	public static void clearCache() {
		memoryCache.clear();
		fileCache.clear();
	}

}
