package com.abscbn.myxph;

import java.io.File;
import java.util.EmptyStackException;

import android.content.Context;

public class FileCache {
    
    private File cacheDir;
    
    public FileCache(Context context){
        //Find the dir to save cached images
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
            cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"MYX PH");
        else
            cacheDir=context.getCacheDir();
        if(!cacheDir.exists())
        {
            boolean b = cacheDir.mkdirs();
            if (!b)
            {
            	 throw new EmptyStackException();
            }
            
         }
    }
    
    public File getFile(String url){
        //I identify images by hashcode. Not a perfect solution, good for the demo.
        String filename=String.valueOf(url.hashCode());
        File f = new File(cacheDir, filename);
        return f;
        
    }
    
    public void clear(){
        File[] files=cacheDir.listFiles();
        for(File f:files)
            f.delete();
    }

}