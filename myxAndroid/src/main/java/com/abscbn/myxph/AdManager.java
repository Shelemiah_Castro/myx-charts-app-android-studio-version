package com.abscbn.myxph;

import android.app.Activity;
import android.os.Handler;
import android.widget.LinearLayout;

import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdRequest.ErrorCode;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.google.ads.doubleclick.DfpAdView;

public class AdManager implements AdListener {
	Activity act;
	AdView adView;
	DfpAdView dfpView;
	SlidingPanel sp;
	Handler ha;
	boolean noDelay, isStarted;

	//private InterstitialAd interstitial;
	//private String intAdId = "a14f30a289ab3fb";

	public String admobId = "a14ee009ff53a39";
	public String idTab = "CF95DC53F383F9A836FD749F3EF439CD";
	public String idLg = "BDC747E055E185C83B565356F1CA31F7";
	public String idHtc = "B378FBAEE1A90C6F3F03FBAD0EEAE069";
	public String idMini = "E92A93B1E42AC6B002A120AFAA127AA0";
	
	private String allId = "/2744311/MyxPh_Mobile_LeaderboardBottom_320x50";
	private String chartId = "/2744311/MyxPh_ChartsListing_Mobile_LeaderboardBottom_320x50";
	private String songId = "/2744311/MyxPh_SongProfile_Mobile_LeaderboardBottom_320x50";
	private String[] dfpIds = {allId, chartId, songId};

	AdManager(Activity a) {
		act = a;
		sp = (SlidingPanel) a.findViewById(R.id.adcont);
		ha = new Handler();
		noDelay = false;
		isStarted = false;
	}

	AdManager(Activity a, boolean l) {
		act = a;
		sp = (SlidingPanel) a.findViewById(R.id.adcont);
		ha = new Handler();
		noDelay = l;
	}

	public void init(int type) {
		isStarted = true;
		//adView = new AdView(act, AdSize.BANNER, admobId);
		
		adView = new DfpAdView(act, AdSize.BANNER, dfpIds[type]);
		adView.setAdListener(this);
		((LinearLayout) act.findViewById(R.id.adspace)).addView(adView);
		//AdRequest request = new AdRequest();
		//request.addTestDevice(idTab);
		//request.addTestDevice(idLg);
		//request.addTestDevice(idHtc);
		//request.addTestDevice(idMini);
		adView.loadAd(new AdRequest());
		//Log.i("myx", "ads init");
		
		//interstitial = new InterstitialAd(act, intAdId);
		//interstitial.setAdListener(this);
		//interstitial.loadAd(request);
	}

	public Runnable dispAd = new Runnable() {
		public void run() {
			//Log.i("myx", "run display ad");
			//try{
				sp.toggle();
				ha.postDelayed(this, 10000);
			//} catch(Exception e) {
			//	Log.i("myx", "dispAd error: " + e.toString());
			//}
		}
	};

	public void destroy() {
		if (isStarted)
			adView.destroy();
	}

	@Override
	public void onDismissScreen(Ad arg0) {
		//Log.i("myx", "onDismissScreen");
	}

	@Override
	public void onFailedToReceiveAd(Ad arg0, ErrorCode errorCode) {
		//Log.i("myx", "failed (" + errorCode + ")");
	}

	@Override
	public void onLeaveApplication(Ad arg0) {
		//Log.i("myx", "onLeaveApplication");
	}

	@Override
	public void onPresentScreen(Ad arg0) {
		//Log.i("myx", "onPresentScreen");
	}

	@Override
	public void onReceiveAd(Ad arg0) {
		//Log.i("myx", "received ad");
		//interstitial.show();
		try {
			if (!noDelay)
				ha.postDelayed(dispAd, 5000);
			else
				ha.post(dispAd);
		} catch (Exception e) {}
	}
}
