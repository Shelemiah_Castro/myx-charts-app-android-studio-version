package com.abscbn.myxph;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedList;
import java.util.Queue;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;

public class ImageHolder {
	private static MemoryCache memoryCache = new MemoryCache();
	private static FileCache fileCache;

	private Handler handler;	
	private Queue<String> https;
	private ImageHttp imageloader;
	private Activity activity;

	public ImageHolder(Activity acty, Handler hand){
		handler = hand;
		activity = acty;
		fileCache = new FileCache(activity);
		https = new LinkedList<String>();
		imageloader = new ImageHttp();
		imageloader.start();
	}

	public void DrawImage(String url, ImageView img, int preLoad) {
		Bitmap b=null;
		File f = fileCache.getFile(url);
		if (f.exists()) {
			b = decodeFile(f);
			if (b != null){
				img.setImageBitmap(b);
				return;
			}			    
		}

		if (url!="")
		{
			if (!https.contains(url))
			{
				synchronized (https) {
					https.add(url);
					https.notifyAll();
				}
			}
		}
		img.setImageResource(preLoad);		
	}

	private static Bitmap decodeFile(File f) {
		try {
			// decode image size
			BitmapFactory.Options o = new BitmapFactory.Options();
			o.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(new FileInputStream(f), null, o);

			// Find the correct scale value. It should be the power of 2.
			final int REQUIRED_SIZE = 70;
			int width_tmp = o.outWidth, height_tmp = o.outHeight;
			int scale = 1;
			while (true) {
				if (width_tmp / 2 < REQUIRED_SIZE
						|| height_tmp / 2 < REQUIRED_SIZE)
					break;
				width_tmp /= 2;
				height_tmp /= 2;
				scale *= 2;
			}

			// decode with inSampleSize
			BitmapFactory.Options o2 = new BitmapFactory.Options();
			o2.inSampleSize = scale;
			return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
		} catch (FileNotFoundException e) {
			//Log.i("MYX Error","e04:"+e.getMessage());
		}
		return null;
	}

	private static Bitmap getBitmap(String url) {
		File f = fileCache.getFile(url);

		// from SD cache
		Bitmap b = decodeFile(f);
		if (b != null)
			return b;

		if (!Utils.isOnline( Utils.getCurrentContext()))
			return null;

		// from web
		try {			
			Bitmap bitmap = null;
			URL imageUrl = new URL(url);
			HttpURLConnection conn = (HttpURLConnection) imageUrl.openConnection();
			conn.setConnectTimeout(30000);
			conn.setReadTimeout(30000);
			InputStream is = conn.getInputStream();
			OutputStream os = new FileOutputStream(f);
			Utils.CopyStream(is, os);
			os.close();
			is.close();
			bitmap = decodeFile(f);
			return bitmap;
		} catch (Exception ex) {
			Log.e("MYX Error","URL: " + url + "| e05:"+ex.getMessage());
			return null;
		}
	}

	public void stopThread() {
		imageloader.interrupt();
	}

	class ImageHttp extends Thread {
		public void run() {
			try {
				while (true) {
					if (https.size() == 0)
						synchronized (https) {
							//Log.v("https:","WAIT");
							https.wait();
						}

					if (https.size() != 0) {
						//Log.v("https:","QS:"+https.size());
						String s;
						synchronized (https) {
							s = https.poll();
						}

						Bitmap bmp = getBitmap(s);
						if (bmp!=null)						
						{						  						
							handler.sendEmptyMessage(0);
						}
						//this.sleep(2000);
					}
					if (Thread.interrupted())
						break;
				}

			}catch (InterruptedException e) {
				//Log.i("MYX Error","e02:"+e.getMessage());
			}
			catch (Exception e){
				//Log.i("MYX Error","e01:"+e.toString());
			}
		}
	}

}
