/**
 * karl tabora
 * abs-cbn interactive
 *
 */
package com.abscbn.myxph;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.RelativeLayout;

import com.vdopia.client.android.VDO.AdEventListener;
import com.vdopia.client.android.VDOAdObject;
import com.vdopia.client.android.VDOBannerView;

public class VdopiaEventListener implements AdEventListener {
	RelativeLayout rl;
	Activity act;
	Context con;
	/*
	VdopiaEventListener(RelativeLayout r) {
		rl = r;
	}
	*/
	public void interstitialWillShow(VDOAdObject object) {
		Log.i("myx", "interstitialWillShow");
	}  
	public void interstitialDidDismiss(VDOAdObject object) {
		Log.i("myx", "interstitialDidDismiss");
	}
	public void noInApp(VDOAdObject object) {
		Log.i("myx", "noInApp");
	}
	@Override
	public void bannerTapEnded(VDOBannerView arg0) {
		Log.i("myx", "bannerTapEnded");
	}
	@Override
	public void bannerTapStarted(VDOBannerView arg0) {
		Log.i("myx", "bannerTapStarted");
	}
	@Override
	public void displayedBanner(VDOBannerView arg0) {
		Log.i("myx", "displayedBanner");
		//rl.setVisibility(View.VISIBLE);
	}
	@Override
	public void noBanner(VDOBannerView arg0) {
		Log.i("myx", "noBanner");
		//rl.setVisibility(View.GONE);
	}
	@Override
	public void noPreApp(VDOAdObject arg0) {
		Log.i("myx", "noPreApp");
	}
	@Override
	public void playedInApp(VDOAdObject arg0) {
		Log.i("myx", "playedInApp");
	}
	@Override
	public void playedPreApp(VDOAdObject arg0) {
		//Log.i("myx", "playedPreApp");
	}
}