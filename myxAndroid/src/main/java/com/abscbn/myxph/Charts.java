package com.abscbn.myxph;
import java.io.Serializable;
import java.util.ArrayList;

public class Charts implements Serializable{
	
    public String chartDate = "";
    public String chartDesc = "";
    public String maxDate = "";
    public String minDate = "";
    public String userHasVoted = "";
    
    public ArrayList<ChartData> myxChart = new ArrayList<ChartData>();
    public ArrayList<ChartData> mitChart = new ArrayList<ChartData>();
    public ArrayList<ChartData> pnyChart = new ArrayList<ChartData>();

}
