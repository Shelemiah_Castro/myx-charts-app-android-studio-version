package com.abscbn.myxph;


public class ChartCategory {
	public int color;
	public int imgLogo;
    public String imgTop;
    public String top3Title;
    public String top3Artist;
 
   
    public ChartCategory(int clr,int iLogo,String sTop,String title, String artist){
    	color = clr;
    	imgLogo=iLogo;
    	imgTop=sTop;
    	top3Title = title;
    	top3Artist = artist;
    }
}