package com.abscbn.myxph;
//SHOW SONG PAGE

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.abs.facebook.FBManager;

public class Preview extends Activity {

	ProgressDialog dialogWait;
	int thisChart = -1;
	int thisPos = -1;
	public ImageHolder imageLoader;
	MediaPlayer mp = new MediaPlayer();
	ChartData td = null;
	ProgressDialog cancelDialog;
	Handler webHandler;
	Handler headHandler;
	Handler refreshData;
	private Handler handler;
	Boolean voted = false;
	private AdManager ads;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		((RelativeLayout) findViewById(R.id.header02)).setVisibility(View.VISIBLE);
		((RelativeLayout) findViewById(R.id.share)).setVisibility(View.GONE);
		ads = new AdManager(this);
		ads.init(2);
		Utils.AnimateHeader(Preview.this);
		LinearLayout header03 = (LinearLayout) findViewById(R.id.header03);
		LayoutInflater inflater = getLayoutInflater();
		header03.addView(inflater.inflate(R.layout.prev, null));
		dialogWait = Utils.CreateWaitProgress(Preview.this);
		
		thisChart = getIntent().getIntExtra("chart", -1);
		thisPos = getIntent().getIntExtra("pos", -1);
		
		handler = new Handler() {
			public void handleMessage(Message msg) {				
				ImageView prvImg = (ImageView) findViewById(R.id.prvImg);				
				imageLoader.DrawImage( Utils.prevImg+Uri.encode(td.img),  prvImg, R.drawable.myxloading ); 	
				/*			
				ImageView prvPreview = (ImageView) findViewById(R.id.prvPreview);
				if (utils.exists(utils.prevMp3 +td.songId+".mp3"))
				    prvPreview.setVisibility(View.VISIBLE);
				else
					prvPreview.setVisibility(View.GONE);
				*/
			}
		};
		
		refreshData = new Handler() {
			public void handleMessage(Message msg) {
				Typeface font = Typeface.createFromAsset(getAssets(), "Anja.ttf");
				TextView myxHead = (TextView) findViewById(R.id.myxHead);
				//App.setVdopiaSdk(Preview.this, Preview.this);
				Utils.checkDateNav(Preview.this);

				//GoogleAnalyticsTracker tracker = GoogleAnalyticsTracker.getInstance();
				//tracker.start("UA-24623266-2", Preview.this);

				ArrayList<ChartData> myxList;
				String sx ="";
				
				if (thisChart == 0) {
					myxList = ChartLoader.myxData.myxChart;									
					myxHead.setText("MYX Hit Chart");
					sx = "/"+ChartLoader.myxData.chartDate+"/myx/";
				} else if (thisChart == 1) {
					myxList = ChartLoader.myxData.mitChart;			
					myxHead.setText("M.I.T. 20");
					sx = "/"+ChartLoader.myxData.chartDate+"/mit/";
				} else {
					myxList = ChartLoader.myxData.pnyChart;			
					myxHead.setText("Pinoy MYX Countdown");
					sx = "/"+ChartLoader.myxData.chartDate+"/pny/";
				}

				if (myxList.size()>0)
				{
					if (myxList.size() < thisPos+1)
						thisPos=0;
					
					td = myxList.get(thisPos);
					MainActivity.tracker.trackPageView("/android"+sx+td.title+" - "+td.artist);
					imageLoader = new ImageHolder(Preview.this, handler);

					RelativeLayout checkOut = (RelativeLayout) findViewById(R.id.extraLayout);
					TextView prvTitle = (TextView) findViewById(R.id.prvTitle);
					TextView prvArtist = (TextView) findViewById(R.id.prvArtist);
					ImageView prvImg = (ImageView) findViewById(R.id.prvImg);
					ImageView prvNum = (ImageView) findViewById(R.id.prvNum);
					ImageView prvVoteOff = (ImageView) findViewById(R.id.voteOff);
					ImageView prvVoteOn = (ImageView) findViewById(R.id.voteOn);
					ImageView prvPreview = (ImageView) findViewById(R.id.prvPreview);
										
					TextView prvAlsoTxt = (TextView) findViewById(R.id.prvAlsoTxt);
					ImageView prvAlsoTop = (ImageView) findViewById(R.id.prvAlsoTop);
					TextView prvAlsoChart = (TextView) findViewById(R.id.prvAlsoChart);
					
					//ImageView prvImg = (ImageView) findViewById(R.id.prvImg);				
					imageLoader.DrawImage( Utils.prevImg+ Uri.encode(td.img),  prvImg, R.drawable.myxloading ); 	

					if (!td.haspreview)
					   prvPreview.setVisibility(View.GONE);
					else
						prvPreview.setVisibility(View.VISIBLE);
					
					//if (td.user_vote == 0) { 
					if (!ChartLoader.myxData.userHasVoted.contains("true")) {
						prvVoteOff.setVisibility(View.GONE);
						prvVoteOn.setVisibility(View.VISIBLE);
					}
					else {
						prvVoteOn.setVisibility(View.GONE);
						prvVoteOff.setVisibility(View.VISIBLE);
					}
						
					prvTitle.setTypeface(font);
					prvArtist.setTypeface(font);
					prvAlsoTxt.setTypeface(font);
					prvAlsoChart.setTypeface(font);

					prvTitle.setText(td.title);
					prvArtist.setText(td.artist);
					
					//Log.i("myx", "vote:" + td.user_vote + ", poll id:" + td.poll_id);
					
					String alsoChart="";
					if (td.alsoChart.equals("myx"))
						alsoChart = "MYX Hit Chart";
					else if (td.alsoChart.equals("mit"))
						alsoChart = "M.I.T. 20";
					else if (td.alsoChart.equals("pny"))
						alsoChart = "Pinoy MYX Countdown";
					else {
						prvAlsoTxt.setVisibility(View.GONE);
						//RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) checkOut.getLayoutParams();
						int top = (int) (10 * Preview.this.getResources().getDisplayMetrics().density + 0.5f);
						int side = (int) (4 * Preview.this.getResources().getDisplayMetrics().density + 0.5f);
						RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.FILL_PARENT,  RelativeLayout.LayoutParams.WRAP_CONTENT);
						params.addRule(RelativeLayout.BELOW, R.id.bench);
						params.setMargins(side, top, side, 0);
						checkOut.setLayoutParams(params);
					}
					
					
					prvAlsoChart.setText(alsoChart);
					try {
						if (td.top!=0){
							//imageLoader.DisplayImage(td.img   , prvImg, R.id.chartTop,  R.drawable.top1loading,0);
							imageLoader.DrawImage( Utils.prevImg + Uri.encode(td.img),  prvImg, R.drawable.myxloading ); 							
							Field field = R.drawable.class.getField("t" + td.top);
							int iconId = field.getInt(null);
							prvNum.setImageResource(iconId);													
						}
						
						if (td.alsoTop!=0){
							Field field = R.drawable.class.getField("t" + td.alsoTop);
							int iconId = field.getInt(null);
							prvAlsoTop.setImageResource(iconId);
						}

					} catch (Exception e) {
						//Log.e("MyTag", "Failure to get drawable id.", e);
					}
					
					if (voted) {
						voted = false;
						if (dialogWait.isShowing())
							dialogWait.hide();
						Utils.alertbox("MYX", "Thanks for voting! Only 1 vote per day is allowed. You can vote again tomorrow from 12:00 AM to 11:59 PM GMT +8:00", Preview.this);
					}
					
				}
			}
		};
								
		webHandler = new Handler() {
			public void handleMessage(Message msg) {
				if (msg.what == 1) {
					refreshData.sendEmptyMessageDelayed(1,100);	
				}
				if (msg.what <= 1) {
					if (dialogWait.isShowing()) {
						dialogWait.dismiss();
					}
					Utils.setOnHeadSwipe(false, 50);
					if (!voted)
						Utils.FlipScreenClose();
				} else if (msg.what == 2) { // show dialog
					if (!dialogWait.isShowing()) {
						dialogWait.show();
					}
					if (!voted)
						Utils.FlipScreenStart(Preview.this);

				} else if (msg.what == 3) { // show no network
					Toast toast = Toast.makeText(getApplicationContext(),
							"Network Unavailable", Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();

				} else if (msg.what == 4) { // show last cached data
					Toast toast = Toast.makeText(getApplicationContext(),
							"Last cached data loaded", Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				} else if (msg.what == 5) { // show last cached data
					Toast toast = Toast.makeText(getApplicationContext(),
							"Loading", Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
			}
		};

		headHandler = new Utils().CreateHeadListener(Preview.this, webHandler);				
		refreshData.sendEmptyMessage(1);
		//ba = new BuzzCityAds(Preview.this);
		//ba.run();
	}
	
	public void leftArrowClick(View view){
		headHandler.sendEmptyMessage(2);
	}
	
	public void rightArrowClick(View view){
		headHandler.sendEmptyMessage(1);
	}
	
	public void openAlso(View view) {
		Toast toast = Toast.makeText(getApplicationContext(),"Loading ...", Toast.LENGTH_SHORT);
		toast.show();
		Intent intent = new Intent(Preview.this, ChartListActivity.class);
		intent.putExtra("chart", Utils.pos(td.alsoChart));
		startActivityForResult(intent, 1);
	}
	
	public void onClickAction(View v) {
		switch (v.getId()) {
		case R.id.voteOff:
			Utils.alertbox("MYX", "Oops! You already voted today. Only 1 vote per day is allowed. You can vote again tomorrow from 12:00 AM to 11:59 PM GMT +8:00", Preview.this);
			break;
		case R.id.voteOn:
			new VoteAsync().execute(td.artist, td.title, Installation.id(this), td.poll_id);
			break;
		case R.id.share:
			//  #Rank on this week's Chart Name
			// Song Title by Artist is #Rank on the Chart Name this week! Chart Link
			String t = "#" + td.top + " on this week's " + Utils.chartTitle[thisChart];
			String d = td.title + " by " + td.artist + " is #" + td.top + " on the " + Utils.chartTitle[thisChart] + " this week! " + Utils.getLink(thisChart);
			//utils.share(this, this, t, d, utils.getLink(thisChart), utils.getLogo(thisChart));
			Utils.share(this, this, t, d, Utils.getLink(thisChart), Utils.getImg(td.img));
			break;
		case R.id.extraLayout:
			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.myxph.com"));
			startActivity(browserIntent);
			break;
		}
	}
	
	public class VoteAsync extends AsyncTask<String, Integer, String> {
		protected void onPreExecute() {
			voted = true;
			if (!dialogWait.isShowing())
				dialogWait.show();
		}
		
		@Override
		protected String doInBackground(String... data) {
			WebService voteService = new WebService(Utils.myxsvc);				
			Map<String, String> params = new HashMap<String, String>();
			
			params.put("singer", data[0]);
			params.put("song_title", data[1]);
			params.put("userID", data[2]);
			params.put("pollID", data[3]);
			return voteService.webGet("/sendVote", params);
		}
		protected void onPostExecute(String res) {
			if (res.contains("true"))
				headHandler.sendEmptyMessage(0);
			else {
				voted = false;
				Utils.alertbox("Voting Failed", "Sorry, but we were unable to process your vote. Please check your connection and try again.", Preview.this);
			}
		}

	}
	
	public void OnClickPreview(View view) {
		if (Utils.isOnline(Preview.this)) {
			createCancelProgressDialog(td.title, td.artist, "Cancel");

			try {
				mp.setDataSource(  Utils.prevMp3 +td.songId+".mp3");
				mp.prepareAsync();				
			} catch (Exception e) {			   
			}

		
			mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {				
				@Override
				public void onPrepared(MediaPlayer mp) {
					mp.start();	
				}
			});
			

			mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
				@Override
				public void onCompletion(MediaPlayer mp) {
					if (cancelDialog!=null){
						cancelDialog.cancel();
					}		
				}
			});
			
			mp.setOnErrorListener( new MediaPlayer.OnErrorListener() {				
				@Override
				public boolean onError(MediaPlayer mp, int what, int extra) {
			        cancelDialog.cancel();	
			        Toast toast = Toast.makeText(getApplicationContext(),
							"Problem encountered while playing", Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
					return false;
				}
			   }
			);

			
		} else {
			Toast toast = Toast.makeText(getApplicationContext(),
					"Network Unavailable", Toast.LENGTH_LONG);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
		}
	}

	@Override
	public void onStop() {
		super.onStop();
		closeMP();

	}

	private void closeMP() {
		if (mp != null) {
			if (mp.isPlaying()) {
				mp.stop();
			}
			mp.release();
			mp = null;
		}
	}

	private void createCancelProgressDialog(String title, String message,
			String buttonText) {
		cancelDialog = new ProgressDialog(this);
		cancelDialog.setTitle(title);
		cancelDialog.setMessage(message);
		
		cancelDialog.setOnShowListener(new DialogInterface.OnShowListener() {
			
			@Override
			public void onShow(DialogInterface dialog) {
			}
		});
		
		cancelDialog.setButton(buttonText,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						closeMP();
						dialog.cancel();
						return;
					}
				});

		cancelDialog
				.setOnCancelListener(new DialogInterface.OnCancelListener() {

					@Override
					public void onCancel(DialogInterface dialog) {
						closeMP();
					}
				});

		cancelDialog.show();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);
	    try {
	    	FBManager.facebook.authorizeCallback(requestCode, resultCode, data);
	    } catch (Exception e) {
	    	Log.i("myx", "facebook callback error");
	    }
	    refreshData.sendEmptyMessageDelayed(1,100);		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return true;
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.findItem(R.id.menuLogout).setVisible(FBManager.isActive());
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menuAbout:
			Utils.alertbox("MYX","Your Choice. Your Music.\nThe #1 music channel in the Philippines.\nhttp://www.myxph.com", this);
			break;
		case R.id.menuClear:			
			ChartLoader.clearCache();
			headHandler.sendEmptyMessage(0);	
			Toast.makeText(this, "Cache Cleared!", Toast.LENGTH_LONG).show();
			break;
		case R.id.menuLogout:
			FBManager.logout(this);
			break;
		}
		return true;
	}
	
}