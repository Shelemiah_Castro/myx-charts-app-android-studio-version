package com.abscbn.myxph;

public class SuperMapWith3Keys<K1, K2, K3, V>
{
	protected SuperMap< V > data = new SuperMap< V >();
	
	public V get( K1 k1, K2 k2, K3 k3 )
	{
		return data.get( k1, k2, k3 );
	}
	
	public boolean put( K1 k1, K2 k2, K3 k3, V v )
	{
		return data.put( k1, k2, k3, v );
	}
}