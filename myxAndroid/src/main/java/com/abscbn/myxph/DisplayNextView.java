package com.abscbn.myxph;

import android.view.animation.Animation;
import android.widget.ImageView;

public final class DisplayNextView implements Animation.AnimationListener {
	private boolean mCurrentView;
	float _halfStart;
	ImageView image1;
	ImageView image2;

	public DisplayNextView(boolean currentView, ImageView image1, ImageView image2,float halfStart) {
		mCurrentView = currentView;
		this.image1 = image1;
		this.image2 = image2;
	    _halfStart = halfStart;
	}

	public void onAnimationStart(Animation animation) {
	}

	public void onAnimationEnd(Animation animation) {
		image1.post(new SwapViews(mCurrentView, image1, image2,_halfStart));
	}

	public void onAnimationRepeat(Animation animation) {
	}
}