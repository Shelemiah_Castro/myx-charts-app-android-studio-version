package com.abscbn.myxph;

public class Alerts {
	 
    public int alertId;
    public String alertText;
    public String alertDate;
 
    @Override
    public String toString()
    {
        return "Alert ID: "+alertId+ " Alert Text: "+alertText+ " Alert Date: "+alertDate;
 
    }
}