package com.abscbn.myxph;

public class ShareItem {
	public final String text;
	public final int icon;
	public ShareItem (String text, Integer icon) {
	    this.text = text;
	    this.icon = icon;
	}
	@Override
	public String toString() {
	    return text;
	}
}