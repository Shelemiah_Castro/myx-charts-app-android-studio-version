package com.abscbn.myxph;

import java.lang.reflect.Field;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class top20Adapter extends ArrayAdapter<ChartData> {
	int resource;
	String response;
	Context context;
	//public ImageLoader imageLoader;
	public ImageHolder imageLoader;
	public Typeface font;
	ListView listview;
	private Handler handler;
	private top20Adapter _this ;
	// Initialize adapter
	
	public top20Adapter(Activity activity, int resource, List<ChartData> items,ListView lv) {
		super(activity, resource, items);
		this.resource = resource;
		_this = this;
		handler = new Handler() {
			public void handleMessage(Message msg) {				
			   _this.notifyDataSetChanged();
			}
		};
		imageLoader = new ImageHolder(activity, handler);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		
		// LinearLayout top3View;
		ChartData al = getItem(position);
		//Log.i("getView Position", ""+position);
		View view = convertView;

		//if (view == null) {
			LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			//if (position == 0)
			//	view = vi.inflate(R.layout.top20b, null);
			/***************************************/
			/*
			if (position < 2) {
				view = vi.inflate(R.layout.top20c, null);
				if (position == 0)
					((ImageView) view.findViewById(R.id.adspace)).setImageResource(R.drawable.samsung_logo);
				else
					((ImageView) view.findViewById(R.id.adspace)).setImageResource(R.drawable.revlon_logo);
			}
			else */
			/***************************************/
				view = vi.inflate(R.layout.top20, null);
		//}
		try {
			ImageView imgTopIcon = (ImageView) view.findViewById(R.id.chartTop20Rank);
			ImageView imgTopImg = (ImageView) view.findViewById(R.id.chartTop20Img);
			
			try {
		       Field field = R.drawable.class.getField("t"+al.top);
		       int iconId = field.getInt(null);		    
			   imgTopIcon.setImageResource(iconId);
			}
			catch (Exception e) {
			    //Log.e("MyTag", "Failure to get drawable id.", e);
			}
			
			imageLoader.DrawImage(Utils.top20Img + Uri.encode(al.img),  imgTopImg, R.drawable.icon );
			//Log.e("MYXPH Image: ", Utils.top20Img + Uri.encode(al.img));
			TextView myxTitle = (TextView)view.findViewById(R.id.chartTop20Title);
			TextView myxArtist = (TextView)view.findViewById(R.id.chartTop20Artist);			
			myxTitle.setText(al.title);
			myxArtist.setText(al.artist);			
			myxTitle.setTypeface(font);
			myxArtist.setTypeface(font);

			//imgLogo.setImageResource(al.imgLogo);		
			//view.setBackgroundColor(al.color);
		} catch (ClassCastException e) {
			throw e;
		}

		// utils.loadImage(imgTop , al.imgTop );
		return view;
	}
}
