package com.abscbn.myxph;


import java.util.ArrayList;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.abs.facebook.FBManager;


//SHOW TOP 20
public class ChartListActivity extends Activity {
	ListView lstTop20;
	top20Adapter arrayAdapter;
	ArrayList<ChartData> top20 = null;
	// ChartWebLoader cwl;
	int thisChart = -1;
	ProgressDialog dialogWait;
	Handler webHandler;
	Handler headHandler;
	Handler refreshData;
	private AdManager ads;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		((RelativeLayout) findViewById(R.id.header02)).setVisibility(View.VISIBLE);
		((RelativeLayout) findViewById(R.id.share)).setVisibility(View.VISIBLE);
		ads = new AdManager(this);
		ads.init(1);
		Utils.AnimateHeader(ChartListActivity.this);
		LinearLayout header03 = (LinearLayout) findViewById(R.id.header03);
		LayoutInflater inflater = getLayoutInflater();
		header03.addView(inflater.inflate(R.layout.list, null));
		dialogWait = Utils.CreateWaitProgress(ChartListActivity.this);
		thisChart = getIntent().getIntExtra("chart", -1);		
		lstTop20 = (ListView) findViewById(R.id.lstText);
		//App.setVdopiaSdk(chartHits.this, chartHits.this);
		
		refreshData = new Handler() {
			public void handleMessage(Message msg) {
				TextView myxHead = (TextView) findViewById(R.id.myxHead);
				//TextView myxDate = (TextView) findViewById(R.id.myxDate);
				//myxDate.setText(utils.getWeek(ChartLoader.myxData.chartDate));
				Utils.checkDateNav(ChartListActivity.this);
				
				//GoogleAnalyticsTracker tracker = GoogleAnalyticsTracker.getInstance();
				//tracker.start("UA-24623266-2", chartHits.this);
				if (thisChart == 0) {
					top20 = ChartLoader.myxData.myxChart;
					myxHead.setText("MYX Hit Chart");
					MainActivity.tracker.trackPageView("/android/"+ChartLoader.myxData.chartDate+"/myx");
				} else if (thisChart == 1) {
					top20 = ChartLoader.myxData.mitChart;
					myxHead.setText("M.I.T. 20");
					MainActivity.tracker.trackPageView("/android/"+ChartLoader.myxData.chartDate+"/mit");
				} else {
					top20 = ChartLoader.myxData.pnyChart;
					myxHead.setText("Pinoy MYX Countdown");
					MainActivity.tracker.trackPageView("/android/"+ChartLoader.myxData.chartDate+"/pny");
				}
				
				//lstTop20.setTag(ChartLoader.myxData.updNum);
				arrayAdapter = new top20Adapter(ChartListActivity.this, R.layout.top20, top20, lstTop20);
				Typeface font = Typeface.createFromAsset(getAssets(), "Anja.ttf");
				arrayAdapter.font = font;
				lstTop20.setAdapter(arrayAdapter);
			}
		};

		refreshData.sendEmptyMessage(1);	
					
		lstTop20.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			public void onItemClick(AdapterView<?> av, View v, int pos, long id) {
				Intent intent = new Intent(ChartListActivity.this, Preview.class);
				intent.putExtra("chart", thisChart);
				intent.putExtra("pos", pos);
				startActivityForResult(intent, 0);
			}
		});

		webHandler = new Handler() {
			public void handleMessage(Message msg) {
				if (msg.what == 1) {
					refreshData.sendEmptyMessageDelayed(1,100);		
				}
				if (msg.what <= 1) {
					if (dialogWait.isShowing()) {
						dialogWait.dismiss();
					}
					Utils.setOnHeadSwipe(false, 50);
					Utils.FlipScreenClose();					
				} else if (msg.what == 2) { // show dialog
					if (!dialogWait.isShowing()) {
						dialogWait.show();
					}
					 Utils.FlipScreenStart(ChartListActivity.this);
				} else if (msg.what == 3) { // show no network
					Toast toast = Toast.makeText(getApplicationContext(),
							"Network Unavailable", Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				} else if (msg.what == 4) { // show last cached data
					Toast toast = Toast.makeText(getApplicationContext(),
							"Last cached data loaded", Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				} else if (msg.what == 5) { // show last cached data
					Toast toast = Toast.makeText(getApplicationContext(),
							"Loading", Toast.LENGTH_LONG);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
			}
		};

		headHandler = new Utils().CreateHeadListener(ChartListActivity.this, webHandler);
		//ba = new BuzzCityAds(chartHits.this);
		//ba.run();
	}
	
	public void leftArrowClick(View view){
		headHandler.sendEmptyMessage(2);
	}
	
	public void rightArrowClick(View view){
		headHandler.sendEmptyMessage(1);
	}
	
	public void onClickAction(View v) {
		switch (v.getId()) {
		case R.id.shareIcon:
			//Headline Chart Name with hyperlink to Chart Link
			//Description: I just checked out the Top 20 videos on the MYX Hit Chart this week!
			
			String t = Utils.chartTitle[thisChart];
			String d = "I just checked out the Top 20 videos on the " + t + " this week! " + Utils.getLink(thisChart);
			Utils.share(this, this, t, d, Utils.getLink(thisChart), Utils.getLogo(thisChart));
			break;
		}
	}
			
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);
	    refreshData.sendEmptyMessageDelayed(1,100);		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return true;
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.findItem(R.id.menuLogout).setVisible(FBManager.isActive());
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menuAbout:
			Utils.alertbox("MYX","Your Choice. Your Music.\nThe #1 music channel in the Philippines.\nhttp://www.myxph.com", this);
			break;
		case R.id.menuClear:			
			ChartLoader.clearCache();
			headHandler.sendEmptyMessage(0);	
			Toast.makeText(this, "Cache Cleared!", Toast.LENGTH_LONG).show();
			break;
		case R.id.menuLogout:
			FBManager.logout(this);
			break;
		}
		return true;
	}

}