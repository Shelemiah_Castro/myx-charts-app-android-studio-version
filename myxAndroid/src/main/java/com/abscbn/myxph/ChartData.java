package com.abscbn.myxph;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ChartData implements Serializable {
	 public int top=0;
     public String img = "";
     public String title = "";
     public String artist = "";
     public String preview = "";
     public int alsoTop = 0;
     public String alsoChart = "";
     public int songId=0;     
     public boolean haspreview=false;
     public String poll_id= "";
}
