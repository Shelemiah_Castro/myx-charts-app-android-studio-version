package com.abscbn.myxph;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


public class ChartCategoryAdapter extends ArrayAdapter<ChartCategory> {

	int resource;
	String response;
	Context context;
	public ImageHolder imageLoader;
    ListView listview;
    public Typeface font;
    Activity act;
	// Initialize adapter
    
    private Handler handler;
    private ChartCategoryAdapter _this ;
    
	public ChartCategoryAdapter(Activity activity, int resource, List<ChartCategory> items, ListView lv) {
		super(activity, resource, items);
		act = activity;
		this.resource = resource;
		_this = this;
		handler = new Handler() {
			public void handleMessage(Message msg) {				
			   _this.notifyDataSetChanged();
			}
		};
		
		imageLoader = new ImageHolder(activity, handler);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ChartCategory al = getItem(position);
		View view = convertView;

		if (view == null) {
			LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			/*if (position == 1)
				view = vi.inflate(App.layout2, null);
			else*/
				view = vi.inflate(MainActivity.layout, null);
		}
		view.setLayoutParams(new ListView.LayoutParams(LayoutParams.FILL_PARENT, MainActivity.div));
		try {
			Log.i("MYX-App", "ChartItem # " + position);
			ImageView imgLogo = (ImageView) view.findViewById(R.id.chartLogo);
			ImageView imgTop = (ImageView) view.findViewById(R.id.chartTop);
			TextView txtTitle = (TextView) view.findViewById(R.id.top3Title);
			TextView txtArtist = (TextView) view.findViewById(R.id.top3artist);
			
			txtTitle.setText(al.top3Title);
			txtArtist.setText(al.top3Artist);
			txtTitle.setTypeface(font);
			txtArtist.setTypeface(font);
			imgTop.setTag(position);
			imageLoader.DrawImage( Utils.top3Img + al.imgTop,  imgTop, R.drawable.myxloading ); 
			imgLogo.setImageResource(al.imgLogo);		
			view.setBackgroundColor(al.color);
		} catch (ClassCastException e) {
			throw e;
		}

		return view;
	}
}
