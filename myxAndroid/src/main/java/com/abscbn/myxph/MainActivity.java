package com.abscbn.myxph;
//SHOW MAIN PAGE

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Display;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.abs.facebook.FBManager;
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
//import com.vdopia.client.android.VDO;
//import com.vdopia.client.android.VDOBannerView;

public class MainActivity extends Activity {
	
	ListView lstTop3;
	ChartCategoryAdapter arrayAdapter;
	ArrayList<ChartCategory> top3s = null;

	ProgressDialog dialogWait;
	int lastClickedY = 0;

	Dialog myxSplash;
	Handler winHandler;	
	Handler myxSplashHandler;
	Handler adsSplashHandler;

	Handler webHandler;
	Handler headHandler;
	Handler refreshData;

	GestureDetector gdt;
	boolean oneTime=false;
	public static int layout, layout2;
	public static GoogleAnalyticsTracker tracker;
	Display display;
	static RelativeLayout toolbar;
	static int th;
	private AdManager ads;
	public static Resources res;
	public static int ctr=0, div=0;
	//public static VDOBannerView banObject;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		Utils.setCurrentContext(MainActivity.this);
		
        tracker = GoogleAnalyticsTracker.getInstance();
        tracker.startNewSession("UA-24623266-2", 15, this);
        res = getResources();
        new FBManager("378013862229809", this);

		display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
		//div = (display.getHeight() < display.getWidth()) ? display.getHeight() / 4 : display.getHeight() / 3;
		layout = R.layout.top3;
		//layout2 = R.layout.top3b;
		ChartLoader.init(MainActivity.this);
		
		myxSplash = new Dialog(MainActivity.this, R.style.Dialog_Fullscreen);

		/***************************************/
		myxSplash.setContentView(R.layout.myxsplashb);
		/***************************************/
		
		myxSplash.setCancelable(false);
		myxSplash.setOnShowListener(new DialogInterface.OnShowListener() {			
			@Override
			public void onShow(DialogInterface dialog) {
				new Handler() {
					public void handleMessage(Message msg) {				
						myxSplash.cancel();
						LoadWindow();
						winHandler.sendEmptyMessageDelayed(1, 100);								
					}
				}.sendEmptyMessageDelayed(1, 3000);
			}
		});
		myxSplash.show();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
	    super.onConfigurationChanged(newConfig);
	    display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
	    if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
	    	div = Utils.getVh(MainActivity.this, display, 1);
	    else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT)
	    	div = Utils.getVh(MainActivity.this, display, 2);
		arrayAdapter = new ChartCategoryAdapter(MainActivity.this, layout, top3s, lstTop3);
	}

	private void LoadWindow() {
		winHandler = new Handler() {
			public void handleMessage(Message msg) {
				setContentView(R.layout.main);
				((RelativeLayout) findViewById(R.id.header02)).setVisibility(View.INVISIBLE);
				((RelativeLayout) findViewById(R.id.share)).setVisibility(View.GONE);
				div = Utils.getVh(MainActivity.this, display, 0);
				ads = new AdManager(MainActivity.this);
				ads.init(0);
				dialogWait = Utils.CreateWaitProgress(MainActivity.this);
				dialogWait.show();

				/***************************************/
				/*
				VDO.initialize("AX123", App.this);
				VDO.setListener(new VdopiaEventListener());
				WebView wv = (WebView) findViewById(R.id.webview);
				setVdopiaWebview(App.this, App.this, wv);
				//setVdopiaSdk(App.this, App.this);
				*/
				/***************************************/
				
				/*TextView myxHead = (TextView) findViewById(R.id.myxHead);
				myxHead.setText("MYX Charts powered by Samsung");
				myxHead.setTextSize(size)*/
				
				LinearLayout header03 = (LinearLayout) findViewById(R.id.header03);
				LayoutInflater inflater = getLayoutInflater();
				//View v = inflater.inflate(R.layout.list, null);
				header03.addView(inflater.inflate(R.layout.list, null));

				lstTop3 = (ListView) findViewById(R.id.lstText);
					
				lstTop3.setTag(0);
				top3s = new ArrayList<ChartCategory>();
				arrayAdapter = new ChartCategoryAdapter(MainActivity.this, layout, top3s, lstTop3);

				Typeface font = Typeface.createFromAsset(getAssets(), "Anja.ttf");
				arrayAdapter.font =font;

				lstTop3.setAdapter(arrayAdapter);
				top3s.add(new ChartCategory(0xffffeb01, R.drawable.myxchartlogo, "","",""));
				top3s.add(new ChartCategory(0xffda4831, R.drawable.mitchartlogo, "","",""));
				top3s.add(new ChartCategory(0xffaf2379, R.drawable.pnychartlogo, "","",""));
				arrayAdapter.notifyDataSetChanged();

				lstTop3.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					public void onItemClick(AdapterView<?> av, View v, int pos,
							long id) {
						if (ChartLoader.myxData!=null) {
							Toast toast = Toast.makeText(getApplicationContext(),"Loading ...", Toast.LENGTH_SHORT);
							toast.setGravity(Gravity.TOP, 0, lastClickedY);
							toast.show();
							Intent intent = new Intent(MainActivity.this, ChartListActivity.class);
							intent.putExtra("chart", pos);
							startActivityForResult(intent, 1);
						}
					}
				});

				lstTop3.setOnTouchListener(new OnTouchListener() {
					public boolean onTouch(View v, MotionEvent event) {
						lastClickedY = (int) event.getRawY();
						return false;
					}
				});


				webHandler = new Handler() {
					public void handleMessage(Message msg) {
						if (msg.what == 1) {
							refreshData.sendEmptyMessageDelayed(1,100);		
						}

						if(msg.what<=1){		
							if (dialogWait.isShowing())							
								dialogWait.dismiss();								

							if (!oneTime)							
								oneTime=true;									

							Utils.setOnHeadSwipe(false, 50);							
							Utils.FlipScreenClose();
						} 
						else if (msg.what==2) { // show dialog
							if (!dialogWait.isShowing())															 
								dialogWait.show();

							if (oneTime){
								Utils.FlipScreenStart(MainActivity.this);								
							}

						} else if (msg.what==3) { // show no network
							Toast toast = Toast.makeText(getApplicationContext(),"Network Unavailable", Toast.LENGTH_LONG);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();

						} else if (msg.what==4) { // show last cached data 
							Toast toast = Toast.makeText(getApplicationContext(),"Last cached data loaded", Toast.LENGTH_LONG);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();
						} else if (msg.what==5) { // show last cached data 
							Toast toast = Toast.makeText(getApplicationContext(),"Loading", Toast.LENGTH_LONG);
							toast.setGravity(Gravity.CENTER, 0, 0);
							toast.show();
						}					
					}
				};

				headHandler = new Utils().CreateHeadListener(MainActivity.this, webHandler);
				headHandler.sendEmptyMessage(0);						
				Utils.AnimateHeader(MainActivity.this);
				//Log.i("myx", ChartLoader.myxData.maxDate);
				
				//ba = new BuzzCityAds(App.this);
				//ba.run();
				
			};
		};

		refreshData = new Handler() {
			public void handleMessage(Message msg) {
				
				if (ChartLoader.myxData.chartDate!="")	{				
					tracker.trackPageView("/android/"+ChartLoader.myxData.chartDate+"/main");
				}
				
				if (ChartLoader.myxData.myxChart.size()>0){
				  top3s.get(0).imgTop =   Uri.encode(ChartLoader.myxData.myxChart.get(0).img);
				  top3s.get(0).top3Title = ChartLoader.myxData.myxChart.get(0).title;				 
				  top3s.get(0).top3Artist = ChartLoader.myxData.myxChart.get(0).artist;
				}
				else{
					  top3s.get(0).imgTop =   "";
					  top3s.get(0).top3Title = "";
					  top3s.get(0).top3Artist = "";
				}
					
				if (ChartLoader.myxData.mitChart.size()>0){				
			  	  top3s.get(1).imgTop = Uri.encode(ChartLoader.myxData.mitChart.get(0).img);
				  top3s.get(1).top3Title = ChartLoader.myxData.mitChart.get(0).title;
				  top3s.get(1).top3Artist = ChartLoader.myxData.mitChart.get(0).artist;
				}else{
					  top3s.get(1).imgTop =   "";
					  top3s.get(1).top3Title = "";
					  top3s.get(1).top3Artist = "";
				}
				
				if (ChartLoader.myxData.pnyChart.size()>0){
				  top3s.get(2).imgTop = Uri.encode(ChartLoader.myxData.pnyChart.get(0).img);
				  top3s.get(2).top3Title = ChartLoader.myxData.pnyChart.get(0).title;
				  top3s.get(2).top3Artist = ChartLoader.myxData.pnyChart.get(0).artist;
				}else{
					  top3s.get(2).imgTop =   "";
					  top3s.get(2).top3Title = "";
					  top3s.get(2).top3Artist = "";
				}
				
				//lstTop3.setTag(ChartLoader.myxData.updNum);
				arrayAdapter.notifyDataSetChanged();
				Utils.checkDateNav(MainActivity.this);
			}
		};
		
		
	}

	public void leftArrowClick(View view){
		headHandler.sendEmptyMessage(2);
	}

	public void rightArrowClick(View view){
		headHandler.sendEmptyMessage(1);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return true;
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.findItem(R.id.menuLogout).setVisible(FBManager.isActive());
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menuAbout:
			Utils.alertbox("MYX","Your Choice. Your Music.\nThe #1 music channel in the Philippines.\nhttp://www.myxph.com", this);
			break;
		case R.id.menuClear:			
			ChartLoader.clearCache();
			headHandler.sendEmptyMessage(0);	
			Toast.makeText(this, "Cache Cleared!", Toast.LENGTH_LONG).show();
			break;
		case R.id.menuLogout:
			FBManager.logout(this);
			break;
		}
		return true;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		refreshData.sendEmptyMessage(1);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		try {
			//textAdClient.saveExposures();
			//graphicAdClient.saveExposures();
		} catch (Exception e) {
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		try {
			ads.destroy();
			tracker.stopSession();
			//textAdClient.saveExposures();
			//graphicAdClient.saveExposures();
		} catch (Exception e) {
		}
	}
	
	/*
	public static void setVdopiaSdk(Context app, final Activity act) {
		RelativeLayout rl = (RelativeLayout) act.findViewById(R.id.banner);
		VDO.setListener(new VdopiaEventListener(rl));
		final int location = 0;// Placing the banner that expands from top to bottom
		banObject = null;
		String bannerSize = new String(VDO.MINI_VDO_BANNER);

		banObject =  new VDOBannerView(app.getApplicationContext(), bannerSize, location);
		if (banObject == null) {
			Log.e("myx","No banner of the requested size found");
			return;
		}
		Handler refresh = new Handler();
		refresh.postDelayed(new Runnable() {
			public void run() {
				//Log.i("myx", "run ad thread");
				RelativeLayout.LayoutParams p =(RelativeLayout.LayoutParams)banObject.getLayoutParams();
				//if(location != 1)
				//	p.addRule(RelativeLayout.ALIGN_PARENT_TOP);

				ViewGroup myLayout = (ViewGroup) act.findViewById(R.id.banner);
				myLayout.addView(banObject, p);
			}
		}, 3000);
	}
	*/
	public static void setVdopiaWebview(final Context app, final Activity act, WebView wv) {
		wv.clearCache(true);
		wv.getSettings().setJavaScriptEnabled(true);
		wv.setWebChromeClient(new WebChromeClient() {
			@Override
			public boolean onJsAlert(WebView view, String url, String message, final android.webkit.JsResult result) { 
				new AlertDialog.Builder(app)
				.setTitle("MYX Vdopia")
				.setMessage(message)
				.setPositiveButton(android.R.string.ok,
						new AlertDialog.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						result.confirm();
					}
				})
				.setCancelable(false)
				.create()
				.show();
				return true;  
			};  
		});
		wv.setWebViewClient(new WebViewClient() {
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				if (url.startsWith("mailto:")) { 
	                Intent intent = new Intent(Intent.ACTION_SEND, Uri.parse(url)); 
	                //intent.setType("plain/text"); 
	                act.startActivity(intent); 
		        }
				else
		            view.loadUrl(url);
				return true;
			}
		});
		wv.loadUrl("file:///android_asset/index.html");
	}
}