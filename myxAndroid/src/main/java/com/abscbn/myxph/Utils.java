package com.abscbn.myxph;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.TranslateAnimation;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.abs.facebook.FBManager;
import com.google.gson.Gson;

public class Utils {
	GestureDetector headGesture;
	public static int hVal = 0;
	private static final int SWIPE_MIN_DISTANCE = 40;
	private static final int SWIPE_THRESHOLD_VELOCITY = 80;	
	private static boolean onHeadSwipe=false;
	public static boolean gotData=false;
	private static String lastHeadAction="";	
	private static Context currContext;
	static SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
	public static String max = null, min = "01-10-2010";
	final static Random rand = new Random();

	
	public static String top3Img="http://myxph.com/thumbnail.ashx?t=songs&w=200&h=200&p=";
	//public static String top3Img="http://www.myxph.com/songs/";
    //public static String top3Img="http://10.1.25.76/myx/thumbnail.ashx?t=songs&w=100&h=100&p=";
    
    public static String top20Img="http://myxph.com/thumbnail.ashx?t=songs&w=100&h=100&p=";
    //public static String top20Img="http://10.1.25.76/myx/thumbnail.ashx?t=songs&w=62&h=65&p=";
	
    public static String prevImg="http://myxph.com/thumbnail.ashx?t=songs&w=300&h=300&p=";
    //public static String prevImg="http://10.1.25.76/myx/thumbnail.ashx?t=songs&w=142&h=140&p=";
	
	public static String prevMp3="http://www.myxph.com/songs/";
	//public static String prevMp3="http://alpha.myxph.com/songs/";
	
	public static String myxsvc = "http://nokiamusic.myxph.com/myx/myxservices.asmx";
	//public static String myxsvc = "http://nokiamusic.myxph.com/myx/myxph2.asmx";
	//public static String myxsvc = "http://taws01/mxyph/webservice/webservice/myxservices.asmx";
	
	public static String origImg = "http://www.myxph.com/songs/";


	public static void setCurrentContext(Context ctx){
		currContext=ctx;
	}

	public static Context getCurrentContext(){
		return currContext;
	}

	public class HeadListener extends SimpleOnGestureListener {
		Handler handler;

		public HeadListener(Handler hand) {
			handler = hand;
		}

		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE
					&& Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
				handler.sendEmptyMessage(1);
				return false;
			} else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE
					&& Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
				handler.sendEmptyMessage(2);
				return false;
			}
			if (e1.getY() - e2.getY() > SWIPE_MIN_DISTANCE
					&& Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {
				// return false; // Bottom to top
			} else if (e2.getY() - e1.getY() > SWIPE_MIN_DISTANCE
					&& Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {
				// return false; // Top to bottom
			}
			return false;
		}
	}
	private static Dialog spx;
	private static Handler sph;
	private static boolean onsph=false;

	public static String getLastHeadAction()
	{
		return lastHeadAction;
	}

	public Handler CreateHeadListener(Activity a,  Handler w ) {
		final Activity _a = a;
		final Handler _w = w;
		Handler h = new Handler() {

			public void handleMessage(Message msg) {
				if(!Utils.isOnHeadSwipe())
				{
					Utils.setOnHeadSwipe(true, 1);
					String action="";
					if (msg.what==0)
						action="";
					else if (msg.what==1)
						action="N";
					else if (msg.what==2)
						action="P";

					lastHeadAction=action;
					new Utils().CreateWebLoader(_a , _w, action, true);
				}
			}
		};

		HeadListener headListener = new HeadListener(h);
		RelativeLayout header01 = (RelativeLayout) a.findViewById(R.id.header01);
		RelativeLayout header02 = (RelativeLayout) a.findViewById(R.id.header02);
		headGesture = new GestureDetector(headListener);
		header01.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(final View view, final MotionEvent event) {
				if (!Utils.onHeadSwipe) 
					headGesture.onTouchEvent(event);
				return true;
			}
		});
		header02.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(final View view, final MotionEvent event) {
				if (!Utils.onHeadSwipe)
					headGesture.onTouchEvent(event);
				return true;
			}
		});

		return h;
	}

	public static void loadImage(ImageView iv, String location) {
		try {
			URL ulrn = new URL(location);
			HttpURLConnection con = (HttpURLConnection) ulrn.openConnection();
			InputStream is = con.getInputStream();
			Bitmap bmp = BitmapFactory.decodeStream(is);
			if (null != bmp)
				iv.setImageBitmap(bmp);
			else
				System.out.println("The Bitmap is NULL");
			is.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void CopyStream(InputStream is, OutputStream os) {
		final int buffer_size = 1024 * 1024;
		try {
			byte[] bytes = new byte[buffer_size];
			for (;;) {
				int count = is.read(bytes, 0, buffer_size);
				if (count == -1)
					break;
				os.write(bytes, 0, count);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void AnimateHeader(Activity a) {
		//ImageView arrowLeft = (ImageView)a.findViewById(R.id.goLeft);
		//ImageView arrowRight = (ImageView)a.findViewById(R.id.goRight);
		//arrowLeft.setAlpha(150);
		//arrowRight.setAlpha(150);

		TextView myxHead = (TextView) a.findViewById(R.id.myxHead);
		TextView myxDate = (TextView) a.findViewById(R.id.myxDate);

		Typeface font = Typeface.createFromAsset(a.getAssets(), "Debussy.ttf");
		myxHead.setTypeface(font);
		myxDate.setTypeface(font);

		final Random barDuration = new Random();
		final AnimationListener al = new AnimationListener() {
			public void onAnimationStart(Animation animation) {
			}

			public void onAnimationRepeat(Animation animation) {
				int b = barDuration.nextInt(1000 - 300) + 300;
				animation.setDuration(b);
			}

			public void onAnimationEnd(Animation animation) {
			}
		};

		for (int i = 1; i <= 22; i++) {
			int resID = a.getResources().getIdentifier("black" + i, "id", a.getPackageName());
			Animation an = new TranslateAnimation(0f, 0f, -10f, -80f);
			int b = barDuration.nextInt(1000 - 300) + 300;

			an.setDuration(b);
			b = barDuration.nextInt(300 - 100) + 100;
			an.setStartOffset(b);
			an.setRepeatCount(-1);
			an.setRepeatMode(2);
			an.setAnimationListener(al);

			ImageView barY = (ImageView) a.findViewById(resID);
			// ImageView barY = (ImageView)ctx.findViewById(resID);
			barY.startAnimation(an);
		}

	}

	public static boolean isOnline(Context ctx) {

		ConnectivityManager cm = (ConnectivityManager) ctx
		.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return  true;
		}
		return false;

	}

	// public static

	public static boolean GetWebData(Activity a, String action, Handler handler) { // must be
		// called
		// thru
		// aysnctask
		try {
			boolean isOnline = Utils.isOnline(a);
			boolean webFetch = false;
			int sentMsg = -1;
			String max = ChartLoader.GetChartMinMax(ChartLoader.indexType.max);
			if (max.equals("")){				
				action = "";
			}

			if (max.equals("") && isOnline){
				sentMsg = 2;
				handler.sendEmptyMessage(sentMsg);
			} 

			if (action.equals("")) { // if to load current day/week cached data
				if (max.equals(""))
					webFetch = true;
				else {
					ChartLoader.GetChartData(max, true);
					if (isOnline && ChartLoader.myxData != null) {
						SimpleDateFormat postFormater = new SimpleDateFormat("MM-dd-yyyy");
						Date d1 = postFormater.parse(ChartLoader.myxData.chartDate);
						Calendar chartDate = Calendar.getInstance();
						chartDate.setTime(d1);
						Calendar today = Calendar.getInstance();
						long diff = today.getTimeInMillis() - chartDate.getTimeInMillis(); // result in mi
						long days = diff / (24 * 60 * 60 * 1000);

						if (days >= 1 ) {
							sentMsg = 2;
							handler.sendEmptyMessage(sentMsg);
							webFetch = true;
						}
						else{
							sentMsg = 4; //load last cached data
							handler.sendEmptyMessage(sentMsg);
						}
					}
					else if (isOnline && ChartLoader.myxData == null) {
						sentMsg = 2;
						handler.sendEmptyMessage(sentMsg);
						webFetch = true;
					}
				}
			}

			String x = "";
			if (!webFetch) {
				if (ChartLoader.myxData != null) {
					SimpleDateFormat postFormater = new SimpleDateFormat("MM-dd-yyyy");
					Calendar cal = Calendar.getInstance();
					cal.setTime( postFormater.parse(ChartLoader.myxData.chartDate ) );
					
					if (action.equals("N")) {
						cal.add( Calendar.DATE, 1 );
					} else if (action.equals("P")) {
						cal.add( Calendar.DATE, -1 );
					}
					Date d1=cal.getTime();
					x = postFormater.format(d1);
					
					Charts chartCache =  ChartLoader.GetChartData(x, true);
					if (chartCache == null) {
						x = ChartLoader.myxData.chartDate;
						sentMsg = 2;
						handler.sendEmptyMessage(sentMsg);
						webFetch = true;
					}
				}

				if (!webFetch && ChartLoader.myxData != null)
				{
					sentMsg = 2;
					handler.sendEmptyMessage(sentMsg);					
				}
			}

			if (webFetch && isOnline) {
				WebService webService = new WebService(Utils.myxsvc);				
				Map<String, String> params = new HashMap<String, String>();

				params.put("userID", Installation.id(currContext));
				params.put("n", x + "");
				params.put("a", action);
				
				String response = webService.webGet("/getMyxCharts", params);
				Log.i("MYX-Android", "JSON REsponse: " + response);
				if (response != "") {
					try{
						Gson gson = new Gson();
						Charts myxData = gson.fromJson(response, Charts.class);
						if (!myxData.chartDate.equals(""))  {
							ChartLoader.myxData = myxData;
							ChartLoader.SetChartData(ChartLoader.myxData.chartDate,ChartLoader.myxData);
						} else
						{
							handler.sendEmptyMessage(-1);
							return false;
						}
					} catch (Exception e) {
						e.printStackTrace();
						sentMsg = -1; // error
						handler.sendEmptyMessage(sentMsg);
						return false;
					}
				}else if (ChartLoader.myxData != null){
					
					sentMsg = 1; 
					handler.sendEmptyMessage(sentMsg);
					return true;
				}
			}

			if (ChartLoader.myxData != null){
				sentMsg = 1; 
				handler.sendEmptyMessage(sentMsg);
				return true;
			}
			else{
				if (ChartLoader.myxData == null && !isOnline)  
				{
					sentMsg = 3; 
					handler.sendEmptyMessage(sentMsg);
				}
				handler.sendEmptyMessage(-1);
				return false;
			}

		} catch (Exception e) {
			e.printStackTrace();
			handler.sendEmptyMessage(-1);
			return false;
		}
	}

	public class WebLoader extends Thread {
		Handler _handler;
		Activity _activity;
		String _action;

		public WebLoader(Activity activity, Handler handler, String action) {
			_activity = activity;
			_action = action;
			_handler = handler;
		}

		public void run() {
			Utils.GetWebData(_activity, _action, _handler );		
		}

	}

	public WebLoader CreateWebLoader(Activity activity, Handler handler, String action, boolean start) {
		WebLoader w = new WebLoader(activity, handler, action);
		w.setPriority(Thread.NORM_PRIORITY + 1);
		if (start)
			w.start();
		return w;
	}

	public static boolean isOnHeadSwipe(){
		return onHeadSwipe;
	}

	public static void setOnHeadSwipe(boolean b,int m){
		final boolean _b = b;
		final int _m = m;
		new Handler() {
			public void handleMessage(Message msg) {				
				onHeadSwipe= _b;
			}
		}.sendEmptyMessageDelayed(1, _m);
	}

	public static ProgressDialog CreateWaitProgress(Context ctx)
	{
		ProgressDialog dialogWait = ProgressDialog.show(ctx, "","Loading. Please wait...", true);
		dialogWait.setCancelable(true);
		WindowManager.LayoutParams layoutParams = dialogWait.getWindow().getAttributes();
		dialogWait.getWindow().setAttributes(layoutParams);
		dialogWait.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
		dialogWait.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
		dialogWait.dismiss();
		return dialogWait;
	}

	public static void RemoveAppCache(Context context) { 
		try { 
			File dir = context.getCacheDir(); 
			if (dir != null && dir.isDirectory()) { 
				deleteDir(dir); 

			} 
		} catch (Exception e) { 
		} 

	} 

	public static boolean deleteDir(File dir) { 
		if (dir!=null && dir.isDirectory()) { 
			String[] children = dir.list(); 
			for (int i = 0; i < children.length; i++) { 
				boolean success = deleteDir(new File(dir, children[i])); 
				if (!success) { 
					return false; 
				} 
			} 
		} 

		// The directory is now empty so delete it 
		return dir.delete(); 
	} 

	public static void FlipScreenStart(Activity a){
		if (onsph)
			return;
		onsph=true;

		final RelativeLayout header00 = (RelativeLayout)a.findViewById(R.id.header00);
		header00.setDrawingCacheEnabled(true);
		Bitmap scr = Bitmap.createBitmap(header00.getDrawingCache());
		header00.setDrawingCacheEnabled(false);

		spx = new Dialog(a, R.style.Dialog_Fullscreen);
		spx.setContentView(R.layout.flipper);
		spx.setCancelable(true);

		final ImageView img01 = (ImageView)spx.findViewById(R.id.flipFront);
		img01.setImageBitmap(scr);
		final ImageView img02 = (ImageView)spx.findViewById(R.id.flipBack);
		img02.setVisibility(View.GONE);
		//img02.setMaxHeight(scr.getHeight());

		spx.setOnShowListener(new DialogInterface.OnShowListener() {			
			@Override
			public void onShow(DialogInterface dialog) {	
				//90,90 left to right
				//-90,-90 right to left
				float rl=-90;
				if (Utils.getLastHeadAction()=="N")
					rl=90;
				applyRotation(img01,img02,true,0,rl);			

			}
		});	

		sph = new Handler() {
			public void handleMessage(Message msg) {
				float rl=-90;
				if (Utils.getLastHeadAction()=="N")
					rl=90;

				applyRotation(img01,img02,false,0,rl);
				new Handler() {
					public void handleMessage(Message msg) {
						onsph=false;
						spx.dismiss();																	  
					}
				}.sendEmptyMessageDelayed(1, 300);					
			}
		};	

		spx.show();
	}

	public static void FlipScreenClose()
	{
		if (onsph && sph!=null)
			sph.sendEmptyMessageDelayed(1, 600);		
	}

	private static void applyRotation(ImageView image1,ImageView image2,boolean isFirstImage,float start, float end) {
		// Find the center of image
		final float centerX = image1.getWidth() / 2.0f;
		final float centerY = image1.getHeight() / 2.0f;

		// Create a new 3D rotation with the supplied parameter
		// The animation listener is used to trigger the next animation
		final Flip3dAnimation rotation =new Flip3dAnimation(start, end, centerX, centerY);
		rotation.setDuration(200);
		rotation.setFillAfter(true);
		rotation.setInterpolator(new AccelerateInterpolator());
		rotation.setAnimationListener(new DisplayNextView(isFirstImage, image1, image2,end));

		if (isFirstImage)
		{
			image1.startAnimation(rotation);
		} else {
			image2.startAnimation(rotation);
		}

	}

	
	public static boolean exists(String URLName){
	    try {
	      HttpURLConnection.setFollowRedirects(false);	    
	      HttpURLConnection con =
	         (HttpURLConnection) new URL(URLName).openConnection();
	      con.setRequestMethod("HEAD");
	      return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
	    }
	    catch (Exception e) {
	       e.printStackTrace();
	       return false;
	    }
	  }
	
	private static long getDateMili(String a) {
		Date md = null;
		try {
			md = dateFormat.parse(a);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return md.getTime();
	}
	
	public static String getWeek(String d) {
		checkMax();
		Date newDate = new Date(getDateMili(d) + 518400000L);  // 6 * 24 * 60 * 60 * 1000
		return d.replaceAll("-", "/") + " - " + dateFormat.format(newDate).replaceAll("-", "/");
	}
	
	public static boolean toShowArrow(int t, String d) {
		Date nd = (t == 0) ? new Date(getDateMili(d) - 518400000L) : new Date(getDateMili(d) + 518400000L);
		if (t == 0) {
			if (getDateMili(min) < nd.getTime())
				return true;
		}
		else {
			if (getDateMili(max) > nd.getTime()) {
				return true;
			}
		}
		return false;
	}
	
	public static void checkMax() {
			max = (max == null) ? ChartLoader.myxData.chartDate : max;
	}
	
	public static void checkDateNav(Activity a) {
		TextView myxDate = (TextView) a.findViewById(R.id.myxDate);
		myxDate.setText(Utils.getWeek(ChartLoader.myxData.chartDate));
		
		ImageButton prev = (ImageButton) a.findViewById(R.id.date);
		ImageButton next = (ImageButton) a.findViewById(R.id.wikipedia);
		if (!Utils.toShowArrow(0, ChartLoader.myxData.chartDate))
			prev.setVisibility(View.GONE);
		else
			prev.setVisibility(View.VISIBLE);
		if (!Utils.toShowArrow(1, ChartLoader.myxData.chartDate))
			next.setVisibility(View.GONE);
		else
			next.setVisibility(View.VISIBLE);
	}
	
	public static int pos(String s) {
		if (s.equals("myx"))
			return 0;
		else if (s.equals("mit"))
			return 1;
		return 2;
	}
	
	public static int getVh(Activity a, Display d, int ty) {
		hVal = 0;
		int[] arr = {R.id.header01, R.id.header02, R.id.toolbar};
		for (int i = 0; i < 3; i++) {
			final RelativeLayout h = (RelativeLayout) a.findViewById(arr[i]);
	        ViewTreeObserver vto = h.getViewTreeObserver();
	        vto.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
	            @Override
	            public void onGlobalLayout() {
	            	hVal += h.getHeight();
	                ViewTreeObserver obs = h.getViewTreeObserver();
	                obs.removeGlobalOnLayoutListener(this);
	            }
	        });
		}
        if (ty == 0)
        	return (d.getHeight() < d.getWidth()) ? (d.getHeight() - hVal) / 4 : (d.getHeight() - hVal) / 3;
        else if (ty == 1)
        	return (d.getHeight() - hVal) / 4;
        return (d.getHeight() - hVal) / 3;
	}
	
	public static long getRand() {
		rand.setSeed(new Date().getTime());
		return rand.nextLong();
	}
	public static void alertbox(String title, String mymessage, Context app)
	   {
	   new AlertDialog.Builder(app)
	      .setMessage(mymessage)
	      .setTitle(title)
	      .setCancelable(true)
	      .setNeutralButton(android.R.string.ok,
	         new DialogInterface.OnClickListener() {
	         public void onClick(DialogInterface dialog, int whichButton){
	        	 dialog.cancel();
	         }
	         
	         })
	      .show();
	   }
	public static void showToast(Context con, String txt) {
		Toast.makeText(con, txt, Toast.LENGTH_SHORT).show();
	}
	
	public static void share(final Activity act, final Context con, final String title, final String text, final String link, final String img) {
		//Log.i("myx", link + " " + img);
		int ix = text.indexOf(link) < 0 ? text.length() : text.indexOf(link); 
		final String tmp = text.substring(0, ix);
		final ShareItem[] items = {
			    new ShareItem("Facebook", R.drawable.fb_icon),
			    new ShareItem("Choose installed application", android.R.drawable.ic_menu_search)
			};
		ListAdapter adapter = new ArrayAdapter<ShareItem>(con,  android.R.layout.select_dialog_item, android.R.id.text1, items){
		    public View getView(int position, View convertView, ViewGroup parent) {
		        View v = super.getView(position, convertView, parent);
		    	try {
			        TextView tv = (TextView)v.findViewById(android.R.id.text1);
			        Drawable draw = MainActivity.res.getDrawable(items[position].icon);
			        int dp = (int) (45 * MainActivity.res.getDisplayMetrics().density + 0.5f);
			        draw.setBounds(0, 0, dp, dp);
			        tv.setCompoundDrawables(draw, null, null, null);
			        dp = (int) (5 * MainActivity.res.getDisplayMetrics().density + 0.5f);
			        tv.setCompoundDrawablePadding(dp);
		    	} catch (Exception e) {
		    		Log.i("absnews", e.toString());
		    	}
		        return v;
		    }
		};
		new AlertDialog.Builder(con)
		    .setTitle("Share")
		    .setAdapter(adapter, new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int item) {
		        	if (item == 0) {
		        		FBManager.sendtoFacebook(act, con, title, tmp, link, img);
		        	}
		        	else {
		        		final Intent intent = new Intent (Intent.ACTION_SEND);
		        		intent.setType("text/plain");
		        		intent.putExtra(Intent.EXTRA_SUBJECT, title);
		        		//intent.putExtra(Intent.EXTRA_TEXT, title + " " + link);
		        		intent.putExtra(Intent.EXTRA_TEXT, text);
		        		con.startActivity(Intent.createChooser(intent, "Share via installed application"));
		        	}
		        }
		    }).show();
	}
	
	public static String[] chartTitle = {"MYX Hit Chart", "MYX International Top 20", "Pinoy MYX Countdown"};
	public static String[] myxSite = {"myx-hit-chart", "myx-international-top-20", "pinoy-myx-countdown"};
	public static String getLogo(int i) {
		return "http://nokiamusic.myxph.com/myx/chartLogos/" + myxSite[i] + ".png";
	}
	public static String getLink(int i) {
		// http://www.myxph.com/charts/
		return "http://www.myxph.com/charts/" + myxSite[i];
	}
	public static String getImg(String t) {
		return origImg + t;
	}
	
}
