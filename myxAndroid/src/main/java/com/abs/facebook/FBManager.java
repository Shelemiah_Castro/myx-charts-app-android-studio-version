package com.abs.facebook;

import org.json.JSONException;
import org.json.JSONStringer;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import com.abs.facebook.SessionEvents.LogoutListener;
import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.Facebook;
import com.abscbn.myxph.Utils;

public class FBManager {
	public static Facebook facebook;
	private static Handler mHandler = new Handler();
    private SessionListener mSessionListener = new SessionListener();
    private static Context currentContext;
    private static boolean showDisclaimer;
    public static SharedPreferences sharedPrefs;
	
	public FBManager(String app_id, Context con) {
		facebook = new Facebook(app_id);
		SessionStore.restore(facebook, con);
		SessionEvents.addLogoutListener(mSessionListener);
	}

    
	public static void sendtoFacebook(final Activity act, final Context con, String title, String desc, String link, String img){
		sharedPrefs = act.getPreferences(0);
		showDisclaimer = sharedPrefs.getBoolean("com.abscbn.myxph.showDisclaimer", true);
		final Bundle params = new Bundle();
		params.putString("name", title);
		params.putString("picture", img);
		params.putString("link", link);
		params.putString("description", (desc.length() > 600) ? desc.substring(0, 600) + "..." : desc);
		/*
		JSONStringer actions;
		try {
		    actions = new JSONStringer().object()
		                .key("name").value("Download Android app")
		                .key("link").value("https://market.android.com/details?id=com.abs.news").endObject();
		    params.putString("actions", actions.toString());
		} catch (JSONException e) {
		    e.printStackTrace();
		}
		*/
		if (showDisclaimer) {
        	//Log.i("myx", "show disclaimer, isActive: " + isActive());
			new AlertDialog.Builder(con)
			.setMessage("This application will only store and use your credentials for sharing articles you choose." +
					"If you wish to logout or clear the saved credentials, you may do so in the options menu.")
			.setTitle("Facebook login disclaimer")
			.setCancelable(true)
			.setNeutralButton(android.R.string.ok,
				new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton){
					if (isActive()) 
					    facebook.dialog(con, "feed", params, new FacebookDialogListener(con));
					else 
						facebook.authorize(act, new FacebookDialogListener(con, params));
				}}).show();

		    SharedPreferences.Editor editor = sharedPrefs.edit();
		    editor.putBoolean("com.abscbn.myxph.showDisclaimer", false);
		    editor.commit();
		}
		else {
        	//Log.i("myx", "hide disclaimer, isActive: " + isActive());
			if (isActive())
			    facebook.dialog(con, "feed", params, new FacebookDialogListener(con));
			else 
				facebook.authorize(act, new FacebookDialogListener(con, params));
		}
	}
	
	public static void saveSession(Context c) {
		SessionStore.save(facebook, c);
	}
	
	public static boolean isActive() {
		return facebook.isSessionValid();
	}
	
	public static void logout(Context c) {
		currentContext = c;
    	Utils.showToast(c, "Logging out of Facebook...");
        AsyncFacebookRunner asyncRunner = new AsyncFacebookRunner(facebook);
        asyncRunner.logout(c, new LogoutRequestListener());
	}
	
	/***************************************************************/
	private static class SessionStore {
	    private static final String TOKEN = "access_token";
	    private static final String EXPIRES = "expires_in";
	    private static final String KEY = "facebook-session";
	    
	    public static boolean save(Facebook session, Context context) {
	        Editor editor =
	            context.getSharedPreferences(KEY, Context.MODE_PRIVATE).edit();
	        editor.putString(TOKEN, session.getAccessToken());
	        editor.putLong(EXPIRES, session.getAccessExpires());
	        return editor.commit();
	    }

	    public static boolean restore(Facebook session, Context context) {
	        SharedPreferences savedSession =
	            context.getSharedPreferences(KEY, Context.MODE_PRIVATE);
	        session.setAccessToken(savedSession.getString(TOKEN, null));
	        session.setAccessExpires(savedSession.getLong(EXPIRES, 0));
	        return session.isSessionValid();
	    }

	    public static void clear(Context context) {
	        Editor editor = context.getSharedPreferences(KEY, Context.MODE_PRIVATE).edit();
	        editor.clear();
	        editor.commit();
	    }
	}
	/***************************************************************/
	private static class LogoutRequestListener extends BaseRequestListener {
        public void onComplete(String response, final Object state) {
            // callback should be run in the original thread, 
            // not the background thread
            mHandler.post(new Runnable() {
                public void run() {
                    SessionEvents.onLogoutFinish();
                }
            });
        }
    }
	/***************************************************************/
	private class SessionListener implements LogoutListener {
        public void onLogoutBegin() {
        	Log.i("myx", "on logout begin");
        }
        
        public void onLogoutFinish() {
        	Utils.showToast(currentContext, "You have been logged out of Facebook.");
            SessionStore.clear(currentContext);
        }
    }
}
