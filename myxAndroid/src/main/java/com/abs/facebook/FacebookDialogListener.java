package com.abs.facebook;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.abscbn.myxph.Utils;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook.DialogListener;
import com.facebook.android.FacebookError;

/**
 * Skeleton base class for RequestListeners, providing default error 
 * handling. Applications should handle these error conditions.
 *
 */
public class FacebookDialogListener implements DialogListener {
	private boolean auth;
	private Context con;
	private Bundle params;
	
	FacebookDialogListener(Context c) {
		con = c;
		auth = false;
	}
	
	FacebookDialogListener(Context c, Bundle b) {
		con = c;
		params = b;
		auth = true;
	}
	
	public void onComplete(Bundle values) {
        String postId = values.getString("post_id");
		if (auth) {
		    auth = false;
		    FBManager.saveSession(con);
		    FBManager.facebook.dialog(con, "feed", params, this);
		}
		else {
	        if (postId != null)
	            Utils.showToast(con, "Article has been shared!");
		}
    }

    public void onFacebookError(FacebookError e) {
        Utils.showToast(con, "Sorry, an error occurred. The post has been cancelled.");
        Log.i("myx", "fb error: " + e.toString());
        //e.printStackTrace();
    }

    public void onError(DialogError e) {
        Utils.showToast(con, "Sorry, an error occurred. The post has been cancelled.");
        Log.i("myx", "error: " + e.toString());
        //e.printStackTrace();        
    }

    public void onCancel() {
    }
    
}
